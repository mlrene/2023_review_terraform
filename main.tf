terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.31.0"
    }
  }
}

provider "aws" {
  region = "ca-central-1"
}

# # Configuring a backend to store our state files in s3 and a LockID to ensure only one user can "execute to the dynamodb table at a time"

terraform {
  backend "s3" {
    bucket         = "2023-review-statefile-backend"
    key            = "terraform.tfstate"
    region         = "ca-central-1"
    dynamodb_table = "rene-terraform-backend-dynamodb"
    encrypt        = false
  }
}

# Creating a VPC called "my_vpc"
resource "aws_vpc" "my_vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "my_vpc"
  }
}
# Creating a subnet in my_vpc
resource "aws_subnet" "my_vpc_subnet_1" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "ca-central-1a"

  tags = {
    Name = "my_vpc_subnet_1"
  }
}

# Creating a subnet in my default VPC using data source block.

data "aws_vpc" "default_vpc" {
  default = true
}

resource "aws_subnet" "default_vpc_subnet_1" {
  vpc_id            = data.aws_vpc.default_vpc.id
  availability_zone = "ca-central-1d"
  cidr_block        = "172.31.48.0/20"

   tags = {
    Name = "default_vpc_subnet_1"
  }
}